import Vue from 'vue'
import myApp from './myApp.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(myApp),
}).$mount('#myApp')